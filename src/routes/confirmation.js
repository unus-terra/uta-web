import React, { Component } from 'react';
import logo from './../img/logo.png'
import box from './../img/box.png'
import background from './../img/background.png'
require('typeface-saira-stencil-one')

export default class Confirmation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: 0,
            height: 0,
        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        console.log(this.props.match.params)
        console.log(this.props.match.params.code)
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }



    render() {
        const { height } = this.state
        return (
            <div style={{
                display: 'flex',
                height: height,
                justifyContent:'center',
                alignItems:'center',
                flexDirection: 'column',
                backgroundImage: `url(${background})`
            }}>
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundImage: `url(${box})`,
                    backgroundSize: '100% 100%',
                    backgroundRepeat: 'no-repeat',
                }}>
                    <div style={styles.header}>
                        <img alt="App logo" src={logo} style={{ marginTop: 40, height: height / 4, objectFit: 'contain' }} />
                    </div>
                    <div style={styles.form} onSubmit={this.handleSubmit}>
                        <p style={styles.message}> CONGRATULATIONS! YOUR ACCOUNT HAS BEEN CREATED CORRECTLY.  <br /> LOGIN TO THE GAME WITH YOUR CREDENTIALS. </p>
                    </div>
                </div>


            </div>
        )
    }
}

const styles = {
    header: {
        display: 'flex',
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 30,
        marginTop: 40,
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 30,
        width: '70%',
        height:'100%',
        marginTop: 60,
        marginBottom: 100
    },
    message: {
        color: '#9395B0',
        width: '100%',
        textAlign: 'center',
        fontFamily: 'Saira Stencil One',
        textAlignVertical: 'center'
        }
}