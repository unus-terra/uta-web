import React from 'react';
import {Switch, Route } from 'react-router-dom';
import ResetPassword from './resetPassword'
import Confirmation from './confirmation'
//import Route from './routes';


export default function Routes() {
  return (
    <Switch>
    <Route path="/resetPassword/:code" component={ResetPassword} />
    <Route path="/confirmation" component={Confirmation} />
  </Switch>
  );
}