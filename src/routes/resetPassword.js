import React, { Component } from 'react';
import login from './../img/changepsw.png'
import logo from './../img/logo.png'
import box from './../img/box.png'
import background from './../img/background.png'
import config from '../config';
import { handleResponse } from '../_helpers';
require('typeface-saira-stencil-one')

export default class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            confirmPassword: '',
            password: '',
            width: 0,
            height: 0,
            showMessage: false,
            showError: false
        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.triggerChangePassword = this.triggerChangePassword.bind(this)
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }


    handleSubmit = (e) => {
        e.preventDefault()
    }


    triggerChangePassword() {
        const { password, confirmPassword } = this.state
        this.changePassword(this.props.match.params.code, password, confirmPassword)
            .then((response) => {
                console.log(response)
                if (response === false) {
                    // login andato a buon fine
                    this.setState({ showMessage: true, showError: true })
                } else {
                    this.setState({ showMessage: true, showError: false })
                }

            })
            .catch((errors) => {
                // c'è stato un errore, mostra il messaggio
                this.setState({ showMessage: true, showError: true })
                console.log("error")
            })
    }


    changePassword(code, password, passwordConfirmation) {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                code,
                password,
                passwordConfirmation
            })
        };

        return fetch(`${config.api.url}/auth/reset-password`, requestOptions)
            .then(handleResponse)
            .then(response => {
                return response
            })
            .catch((error) => {
                // TODO handle error
                return false
            })
    }


    render() {
        const { confirmPassword, password, showMessage, showError, height } = this.state
        return (
            <div style={{
                display: 'flex',
                height: height,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column',
                backgroundImage: `url(${background})`
            }}>
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundImage: `url(${box})`,
                    backgroundSize: '100% 100%',
                    backgroundRepeat: 'no-repeat',
                }}>
                    <div style={styles.header}>
                        <img alt="App logo" src={logo} style={{ marginTop: 40, height: height / 5, objectFit: 'contain' }} />
                    </div>
                    <form style={styles.form} onSubmit={this.handleSubmit}>
                        {showMessage === true ?
                            showError === true ? <p style={styles.message}> ERROR IN SAVING THE PASSWORD. <br /> PLEASE, TRY AGAIN. </p> :
                                <p style={styles.message}> YOUR NEW PASSWORD HAS BEEN SAVED CORRECTLY. <br /> LOGIN TO THE GAME WITH YOUR NEW CREDENTIALS. </p>
                            :
                            <>
                                <div style={styles.field}>
                                    <p style={styles.label}>NEW PASSWORD</p>
                                    <input
                                        type="password"
                                        name="password"
                                        autoComplete="on"
                                        value={password}
                                        onChange={(event) => this.setState({ password: event.target.value })}
                                        placeholder="New password"
                                        style={styles.textInput}
                                    />
                                </div>
                                <div style={styles.field}>
                                    <p style={styles.label}>CONFIRM PASSWORD</p>
                                    <input
                                        type="password"
                                        name="confirmPassword"
                                        autoComplete="on"
                                        value={confirmPassword || ""}
                                        onChange={(event) => this.setState({ confirmPassword: event.target.value })}
                                        placeholder="Confirm Password"
                                        style={styles.textInput}
                                    />
                                </div>
                                {confirmPassword !== "" && password !== confirmPassword && <div style={{ width: '100%', color: '#BE4A4A', fontSize: 11, marginTop: 5, fontFamily: 'Saira Stencil One', textAlign: 'left' }}>Passwords don't match.</div>}
                                <div style={styles.button}>
                                    <button
                                        onClick={() => this.triggerChangePassword()}
                                        style={{ border: 'none', backgroundColor: 'transparent', outline: 'none' }}
                                        disabled={!confirmPassword || !password || password !== confirmPassword}>
                                        <img alt="Change Psw button" src={login} style={{ width: '100%', objectFit: 'contain' }} /></button>
                                </div>
                            </>}
                    </form>
                </div>
            </div>
        )
    }
}

const styles = {
    header: {
        display: 'flex',
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 30,
        marginTop: 40,
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 30,
        width: '60%',
        height: '100%',
        marginTop: 30,
        marginBottom: 80
    },
    textInput: {
        borderRadius: 30,
        width: '100%',
        border: 0,
        padding: 18,
        outline: 'none'
    },
    button: {
        display: 'flex',
        flex: 2,
        justifyContent: 'center',
        marginTop: 40
    },
    field: {
        display: 'flex',
        flex: 1,
        marginTop: 10,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    },
    label: {
        color: '#9395B0',
        flex: 1,
        width: '100%',
        textAlign: 'left',
        fontFamily: 'Saira Stencil One',
        fontSize: 20
    },
    password: {
        color: '#FFFFFF',
        fontStyle: 'italic',
        fontSize: 13
    },
    message: {
        color: '#9395B0',
        flex: 1,
        width: '100%',
        textAlign: 'center',
        fontFamily: 'Saira Stencil One',
        textAlignVertical: 'center',
    }
}