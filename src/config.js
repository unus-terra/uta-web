let config = {
    environment: process.env.NODE_ENV || 'development',
    api: {
      url: process.env.REACT_APP_API_URL || 'https://unusterra.herokuapp.com'  //'http://localhost:1337' 
    },
  }
  
  module.exports = config
  