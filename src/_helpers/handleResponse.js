
export function handleResponse(response) {
  console.log('[_helpers][handleResponse] response', response)
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if ([401, 403].indexOf(response.status) !== -1) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
               console.log("Forbidden")
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

export function handleFailure (error) {
  console.log('[_helpers][handleRhandleFailureesponse] error', error)
  // TODO implement some error handling
  return error
}